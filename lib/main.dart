import 'package:flutter/material.dart';
enum APP_THEME{LIGHT,DARK}

void main() {
  runApp(ContacctProfilePage());
}

class MyAppTheme{
  static ThemeData appThemeLight(){
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
            color: Colors.orange,
            iconTheme: IconThemeData(
              color: Colors.orange.shade50,
            )
        ),
        iconTheme: IconThemeData(
            color: Colors.indigo.shade500
        )
    );
  }

  static ThemeData appThemeDark(){
    return ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
            color: Colors.orange,
            iconTheme: IconThemeData(
              color: Colors.orange.shade50,
            )
        ),
        iconTheme: IconThemeData(
            color: Colors.indigo.shade500
        )
    );
  }
}


class ContacctProfilePage extends StatefulWidget {
  @override
  State<ContacctProfilePage> createState() => _ContacctProfilePageState();
}

class _ContacctProfilePageState extends State<ContacctProfilePage> {
  var currentTheme = APP_THEME.LIGHT;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
      ? MyAppTheme.appThemeLight() : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppbarWidget(),
        body: buildbodyWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.threesixty,color: Colors.white,size: 50),backgroundColor: Colors.pinkAccent,
          onPressed: () {
            setState(() {
              currentTheme == APP_THEME.DARK
                  ? currentTheme = APP_THEME.LIGHT : currentTheme = APP_THEME.DARK;
            });
          },
        ),
      ),
    );
  }
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,

        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,

        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,

        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,

        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,

        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

Widget mobilePhoneListtile() {
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("091-025-5312"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget mobile2PhoneListtile() {
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("080-031-4720"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget emailListtile() {
  return ListTile(
    leading: Icon(Icons.email),
    title: Text("63160201@go.buu.ac.th"),
    subtitle: Text("work"),
    trailing: IconButton(
      icon: Icon(null),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget addressListtile() {
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text("ไม่มีบ้าน"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

AppBar buildAppbarWidget() {
  return AppBar(
    backgroundColor: Colors.yellow.shade300,
    leading: Icon(
      Icons.arrow_back,
      color: Colors.black,
    ),
    actions: <Widget>[
      IconButton(
          icon: Icon(Icons.star_border),
          color: Colors.black,
          onPressed: () {
            print("Contact is starred");
          }),
    ],
  );
}

Widget buildbodyWidget() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            width: double.infinity,

            //Height constraint at Container widget level
            height: 400,
            child: Image.network(
              "https://media.tenor.com/gQV5VzHLWQIAAAAM/among-us-sus.gif",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text("Narachai Praphaisan",
                      style: new TextStyle(
                          fontSize: 30.0,
                          fontWeight: FontWeight.bold,
                          foreground: Paint()
                            ..shader = LinearGradient(
                              colors: <Color>[
                                Colors.pinkAccent,
                                Colors.deepPurpleAccent,
                                Colors.red
                                //add more color here.
                              ],
                            ).createShader(Rect.fromLTWH(
                                0.0, 0.0, 200.0, 100.0))) //Priyanke Tyagi

                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          Container(
            margin: const EdgeInsets.only(top: 8, bottom: 8),
            child: Theme(
              data: ThemeData(
                iconTheme: IconThemeData(
                  color: Colors.tealAccent.shade700,
                ),
              ),
              child: profileActionItems(),
  ),


          ),
          Divider(
            color: Colors.grey,
          ),
          mobilePhoneListtile(),
          mobile2PhoneListtile(),
          Divider(
            color: Colors.grey,
          ),
          emailListtile(),
          Divider(
            color: Colors.grey,
          ),
          addressListtile(),
        ],
      ),
    ],
  );
}

Widget profileActionItems(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton(),
    ],
  );
}
